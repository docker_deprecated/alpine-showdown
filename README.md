# alpine-showdown

#### [alpine-x64-showdown](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-showdown/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-showdown.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-showdown "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-showdown.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-showdown "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-showdown.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-showdown/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-showdown.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-showdown/)
#### [alpine-aarch64-showdown](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-showdown/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64/alpine-aarch64-showdown.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-showdown "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64/alpine-aarch64-showdown.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-showdown "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-showdown.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-showdown/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-showdown.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-showdown/)
#### [alpine-armhf-showdown](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-showdown/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-showdown.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-showdown "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-showdown.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-showdown "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-showdown.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-showdown/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-showdown.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-showdown/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [ShowDown](https://iodides.tistory.com/8?category=693844/)
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 4040:4040/tcp \
           -v /conf.d:/conf.d \
           forumi0721alpinex64/alpine-x64-showdown:latest
```

* aarch64
```sh
docker run -d \
           -p 4040:4040/tcp \
           -v /conf.d:/conf.d \
           forumi0721alpineaarch64/alpine-aarch64-showdown:latest
```

* armhf
```sh
docker run -d \
           -p 4040:4040/tcp \
           -v /conf.d:/conf.d \
           forumi0721alpinearmhf/alpine-armhf-showdown:latest
```



----------------------------------------
#### Usage

* Run serer and execute `showdown`.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 4040/tcp           | ShowDown port                                    |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Config data                                      |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| TRANSMISSION_URL   | Transmission RPC url                             |
| TRANSMISSION_USERNAME | Transmission username                         |
| TRANSMISSION_PASSWORD | Transmission password                         |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-showdown](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-showdown/)
* [forumi0721alpineaarch64/alpine-aarch64-showdown](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-showdown/)
* [forumi0721alpinearmhf/alpine-armhf-showdown](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-showdown/)

